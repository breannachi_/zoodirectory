package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

public class AnimalDetailActivity extends AppCompatActivity {
    private TextView description;
    private TextView name;
    private ImageView image;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_detail_layout);
        description = (TextView) findViewById(R.id.description);
        name = (TextView) findViewById(R.id.name);
        image = (ImageView) findViewById(R.id.image);

        Intent myCallerIntent = getIntent();
        Bundle myBundle = myCallerIntent.getExtras();
        String animalName = myBundle.getString("name");
        String animalDesc = myBundle.getString("desc");
        int position = myBundle.getInt("position");
        int animalId = myBundle.getInt("id");

        if (position == 4){
            DialogFragment fragment = new MyDialogFragment();
            fragment.show(getSupportFragmentManager(), "warning");
            name.setText(animalName);
            description.setText(animalDesc);
            image.setImageResource(animalId);
        }
        else {
            name.setText(animalName);
            description.setText(animalDesc);
            image.setImageResource(animalId);
        }
    }

    //action bar:
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.action_zooInfo){
            Intent intent = new Intent(this, ZooInformationActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_unInstall){
            String packageName = "package:edu.sjsu.android.zoodirectory";
            Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse(packageName));
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

