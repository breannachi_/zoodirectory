package edu.sjsu.android.zoodirectory;

public class Animal {
    private String name;
    private int nameId;
    private String description;

    public Animal(String imageName, int id, String des){
        name = imageName;
        nameId = id;
        description = des;
    }
    public String getName(){
        return name;
    }
    public int getId(){
        return nameId;
    }
    public String getDescription(){
        return description;
    }

}
