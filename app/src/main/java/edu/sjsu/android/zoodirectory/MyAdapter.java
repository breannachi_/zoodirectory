package edu.sjsu.android.zoodirectory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Animal> animals;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtHeader;
        private ImageView image;
        private View layout;

        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) v.findViewById(R.id.name);
            image = (ImageView) v.findViewById(R.id.icon);
            layout = v;
        }
    }

    public MyAdapter(List<Animal> images, Context c){
        animals = new ArrayList<Animal>();
        for (int i = 0; i < images.size(); i++){
            animals.add(images.get(i));
        }
        context = c;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        String name = animals.get(position).getName();
        int animalId = animals.get(position).getId();

        holder.txtHeader.setText(name);
        holder.image.setImageResource(animalId);

        holder.layout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(context, AnimalDetailActivity.class);
                Bundle myBundle = new Bundle();
                myBundle.putString("name", name);
                myBundle.putInt("position", position);
                myBundle.putString("desc", animals.get(position).getDescription());
                myBundle.putInt("id", animalId);
                intent.putExtras(myBundle);
                context.startActivity(intent);}

        });
    }

    @Override
    public int getItemCount(){
        return animals.size();
    }
}
