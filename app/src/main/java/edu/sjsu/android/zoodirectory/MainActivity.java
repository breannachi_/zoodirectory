package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Animal> animalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //dialog fragment:

        //recycler view:
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        animalList = new ArrayList<>();
        addAnimalList();

        mAdapter = new MyAdapter(animalList, this);
        recyclerView.setAdapter(mAdapter);
    }

    public void addAnimalList(){
        //Descriptions are from Wikipedia
        animalList.add(new Animal("Charmander", R.drawable.charmander, "Charmander (Japanese: ヒトカゲ Hitokage) is a Fire-type Pokémon introduced in Generation I. It evolves into Charmeleon starting at level 16, which evolves into Charizard starting at level 36. Along with Bulbasaur and Squirtle, Charmander is one of three starter Pokémon of Kanto available at the beginning of Pokémon Red, Green, Blue, FireRed, and LeafGreen."));
        animalList.add(new Animal ("Dragonair", R.drawable.dragonair, "Dragonair (Japanese: ハクリュー Hakuryu) is a Dragon-type Pokémon introduced in Generation I.It evolves from Dratini starting at level 30 and evolves into Dragonite starting at level 55."));
        animalList.add(new Animal ("Eevee", R.drawable.eevee, "Eevee is the game mascot and starter Pokémon in Pokémon: Let's Go, Eevee!, as well as for the main characters of Pokémon XD: Gale of Darkness and Pokémon Conquest. It is the starting Pokémon and first Pokémon employee of the player in Pokémon Café Mix. It is also the rival's starter Pokémon in Pokémon Yellow, although Professor Oak originally intended to give it to the player."));
        animalList.add(new Animal("Vulpix", R.drawable.vulpix, "Vulpix (Japanese: ロコン Rokon) is a Fire-type Pokémon introduced in Generation I. It evolves into Ninetales when exposed to a Fire Stone. In Alola, Vulpix has an Ice-type regional form. It evolves into Alolan Ninetales when exposed to an Ice Stone."));
        animalList.add(new Animal("Snorlax", R.drawable.snorlax, "It is not satisfied unless it eats over 880 pounds of food every day. When it is done eating, it goes promptly to sleep."));
    }

    //action bar:
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.action_zooInfo){
            Intent intent = new Intent(this, ZooInformationActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_unInstall){
            String packageName = "package:edu.sjsu.android.zoodirectory";
            Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse(packageName));
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}