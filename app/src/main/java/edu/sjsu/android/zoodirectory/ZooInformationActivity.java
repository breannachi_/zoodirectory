package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ZooInformationActivity extends AppCompatActivity {
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoo_information);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "tel:888-8888";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(number));
                startActivity(intent);
            }
        });
    }
    //action bar:
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.action_zooInfo){
            Intent intent = new Intent(this, ZooInformationActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_unInstall){
            String packageName = "package:edu.sjsu.android.zoodirectory";
            Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse(packageName));
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}